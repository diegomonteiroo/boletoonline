package br.com.alelo.boleto_online.ports;


public interface IReceiverPort {
    void receive(String message);
}