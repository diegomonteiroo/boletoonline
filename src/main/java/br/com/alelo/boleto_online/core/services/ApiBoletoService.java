package br.com.alelo.boleto_online.core.services;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.alelo.boleto_online.adapters.SenderAdapterCreateBB;
import br.com.alelo.boleto_online.adapters.SenderAdapterCreateBrad;
import br.com.alelo.boleto_online.core.domain.Payment;
import br.com.alelo.boleto_online.core.domain.dto.ApiBoletoBBRequestDTO;
import br.com.alelo.boleto_online.core.domain.enums.StatusEnum;
import br.com.alelo.boleto_online.core.repositories.ApiBoletoRepository;

@Service
public class ApiBoletoService {
	@Autowired
	private ApiBoletoRepository apiRepository;
	
	@Autowired
	private SenderAdapterCreateBrad senderCreateBrad;
	
	@Autowired
	private SenderAdapterCreateBB senderCreateBB;
		
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiBoletoService.class);
	
	public Payment filter(Payment payment) {
		
		switch (payment.getProvider().getProvider()) {
		case "001":
			LOGGER.info("Banco do Brasil");
			payment.setStatus(StatusEnum.PENDING);
			payment = apiRepository.save(payment);
			if(Objects.nonNull(payment)) {
				LOGGER.info("Encaminhando messagem para a fila \n");
				sendInputNewBB(payment);
			}else {
				receiveMessageErrorBB("Erro ao salvar na base");
				LOGGER.error("Erro ao salvar na base de dados");
			}
			return payment;
		case "237":
			LOGGER.info("Banco Bradesco");
			payment.setStatus(StatusEnum.PENDING);
			payment = apiRepository.save(payment);
			if(Objects.nonNull(payment)) {
				LOGGER.info("Encaminhando messagem para a fila \n");
				sendInputNewBrad(payment);
			}else {
				receiveMessageErrorBrad("Erro ao salvar na base");
				LOGGER.error("Erro ao salvar na base de dados");
			}
			return payment;
		default :
			LOGGER.info("Banco não encontrado");
			return null;
		}
	}

	public void sendInputNewBB(Payment boleto) {
		try {
			ApiBoletoBBRequestDTO apiBoletoBBDTO = new ApiBoletoBBRequestDTO(boleto);
			Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
			senderCreateBB.send(gson.toJson(apiBoletoBBDTO));
		} catch(Exception e) {
			LOGGER.error("Erro ao converter para JSON");
		}
	}
	
	public void receiveMessageSuccessBB(String message) {
		//LOGGER.info(MENSAGEM + message);
	}	
	
	public void receiveMessageErrorBB(String message) {
		//LOGGER.info(MENSAGEM + message);
	}
	
	public void sendInputNewBrad(Payment boleto) {
		try {
		//	apiBoletoBradescoRequestDTO = new ApiBoletoBradescoRequestDTO(boleto);
			Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
			//senderCreateBrad.send(gson.toJson(apiBoletoBradescoRequestDTO));
		} catch (Exception e) {
			LOGGER.error("Erro ao converter para JSON " + e.getLocalizedMessage());
		}
	}
	
	public void receiveMessageSuccessBrad(String message) {
		//LOGGER.info(MENSAGEM + message);
	}
	
	public void receiveMessageErrorBrad(String message) {
		//LOGGER.info(MENSAGEM + message);
	}
}