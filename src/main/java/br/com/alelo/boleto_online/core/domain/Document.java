package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "document")
public class Document implements Serializable {

	private static final long serialVersionUID = -2402391828136931735L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ApiModelProperty(dataType = "Integer", notes = "Indicar se o documento é CPF ou CNPJ", example = "1 = CPF - 2 = CNPJ", required = true)
	private Integer type;
	
	@ApiModelProperty(dataType = "Long", notes = "Informar CPF ou CNPJ", example = "99999999999 ou 99999999/9999")
	@SerializedName(value = "number")
	private String streetNumber;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Customer customer;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Guarantor guarantor;

	public Document(Integer id, Integer type, String streetNumber, Customer customer, Guarantor guarantor) {
		super();
		this.id = id;
		this.type = type;
		this.streetNumber = streetNumber;
		this.customer = customer;
		this.guarantor = guarantor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}
}