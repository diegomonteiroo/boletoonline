package br.com.alelo.boleto_online.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import br.com.alelo.boleto_online.core.services.ApiBoletoService;
import br.com.alelo.boleto_online.ports.IReceiverPort;

public class ReceiverAdapterBBError implements IReceiverPort{

	@Autowired
	private ApiBoletoService apiBoletoService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverAdapterBBError.class);
	
	@Override
	@JmsListener(destination = "${application.fila.jms.bnk_register_bb_error}")
	public void receive(String message) {
		try {
			LOGGER.info("Mensagem recibida da FILA: bnk_register_bb_error \n Mensagem: " + message);
			//apiBoletoService.receiveMessageErrorBB(message);
		} catch (Exception error) {
			LOGGER.error("Erro ao enviar mensagem para o serviço " + error.getLocalizedMessage());
		}
	}
}