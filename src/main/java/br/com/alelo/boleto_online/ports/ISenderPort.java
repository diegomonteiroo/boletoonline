package br.com.alelo.boleto_online.ports;

public interface ISenderPort {
    void send (String message);
}
