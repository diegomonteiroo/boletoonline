package br.com.alelo.boleto_online.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import br.com.alelo.boleto_online.core.services.ApiBoletoService;
import br.com.alelo.boleto_online.ports.IReceiverPort;

public class ReceiverAdapterBradSuccess implements IReceiverPort {

	@Autowired
	private ApiBoletoService apiBoletoService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverAdapterBradSuccess.class);
	
	@Override
	@JmsListener(destination = "${application.fila.jms.bnk_register_brad_success}")
	public void receive(String message) {
		try {
			LOGGER.info("Mensagem recebida da FILA: bnk.register.brad.success \n Mensagem: " + message);
		//	apiBoletoService.receiveMessageSuccessBrad(message);
		} catch(Exception error) {
			LOGGER.error("Erro ao enviar mensagem para o serviço " + error.getLocalizedMessage());
		}
	}
}