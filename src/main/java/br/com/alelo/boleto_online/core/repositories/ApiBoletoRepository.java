package br.com.alelo.boleto_online.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.alelo.boleto_online.core.domain.Payment;

@Repository
public abstract interface ApiBoletoRepository extends JpaRepository<Payment, Integer>{

}