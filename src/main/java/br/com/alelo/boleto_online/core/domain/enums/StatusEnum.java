package br.com.alelo.boleto_online.core.domain.enums;

public enum StatusEnum {
	PENDING(1),
	REGISTEERED(2),
	ERROR(3);
	
	private Integer status;
	
	StatusEnum(Integer status) {
		this.status = status;
	}

	StatusEnum getStatus(Integer status) {
		for(StatusEnum name : StatusEnum.values()) {
			if(name.status.equals(status)) {
				return name;
			}
		}
		return null;
	}

	Integer getStatus() {
		return status;
	}

	void setStatus(Integer status) {
		this.status = status;
	}
}