package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 511056690495496959L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ApiModelProperty(dataType = "String", notes = "Informar o nome do cliente", example = "Claudineide Soares", required = true)
	private String name;
	
	@ManyToOne
	@JsonBackReference(value = "payment-customer")
	private Payment payment;
	
	@OneToOne(mappedBy = "customer")
	private Address address;
	
	@OneToOne(mappedBy = "customer")
	private Document document;
	
	public Customer(String name, Address address, Document document) {
		this.name = name;
		this.address = address;
		this.document = document;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
}