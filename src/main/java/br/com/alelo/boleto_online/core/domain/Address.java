package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "address")
public class Address implements Serializable{

	private static final long serialVersionUID = 7467849610357845772L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ApiModelProperty(dataType = "String", notes = "Informar o endereço.", example = "Rio de Janeiro", required = true)
	private String street;
	
	@ApiModelProperty(dataType = "String", notes = "Informar o numero da residencia.", example = "100", required = true)
	@SerializedName(value = "number")
	private String streetNumber;
	
	@ApiModelProperty(dataType = "Integer", notes = "Informar código postal.", example = "01001000", required = true)
	private Integer zipcode;
	
	@ApiModelProperty(dataType = "String", notes = "Informar complemento do endereço.", example = "AP 18", required = true)
	private String complement;
	
	@ApiModelProperty(dataType = "String", notes = "Informar o município.", example = "Rio de Janeiro", required = true)
	private String district;
	
	@ApiModelProperty(dataType = "String", notes = "Informar a cidade.", example = "Rio de Janeiro", required = true)
	private String city;
	
	@ApiModelProperty(dataType = "String", notes = "Informar o estado.", example = "Rio de Janeiro", required = true)
	private String state;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Guarantor guarantor;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Customer customer;

	public Address(Integer id, String street, String streetNumber, Integer zipcode, String complement, String district,
			String city, String state, Guarantor guarantor, Customer customer) {
		super();
		this.id = id;
		this.street = street;
		this.streetNumber = streetNumber;
		this.zipcode = zipcode;
		this.complement = complement;
		this.district = district;
		this.city = city;
		this.state = state;
		this.guarantor = guarantor;
		this.customer = customer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public Integer getZipcode() {
		return zipcode;
	}

	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}