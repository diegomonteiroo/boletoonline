package br.com.alelo.boleto_online.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;

import br.com.alelo.boleto_online.ports.ISenderPort;

public class SenderAdapterCreateBrad implements ISenderPort {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Value("${application.fila.jms.bnk_register_brad_new}")
	private String filaRegisterBradNew;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SenderAdapterCreateBrad.class);
	
	@Override
	public void send(String message) {
		LOGGER.info("Passando novo boleto para a fila: bnk.register.brad.new");
		jmsTemplate.convertAndSend(filaRegisterBradNew, message);
		LOGGER.info("Passou a seguinte mensagem: \n" + message);
	}
}