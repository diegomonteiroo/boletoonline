package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.gson.annotations.SerializedName;

import br.com.alelo.boleto_online.core.domain.enums.StatusEnum;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "payment")
public class Payment implements Serializable {

	private static final long serialVersionUID = 2402998127962693685L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ApiModelProperty(dataType = "String", notes = "Origim da plataforma que esta querendo registrar o boleto", required = true)
	private String clientid;
	
	@ApiModelProperty(dataType = "String", notes = "Chave Pública para Autenticação Dupla na Alelo", required = true)
	private String clientsecret;
	
	@ApiModelProperty(dataType = "String", notes = "Produto da plataforma que está querendo registrar o Boleto", required = true)
	@SerializedName(value = "origin-product")
	private String originProduct;
	
	@ApiModelProperty(dataType = "String", notes = "Número de identificação da origem referente ao pedido de registro do boleto", required = true)
	@SerializedName(value = "origin-request-id")
	private String originRequestid;

	@OneToOne(mappedBy = "payment")
	private Provider provider;
	
	@ApiModelProperty(dataType = "String", notes = "Número do Boleto", required = true)
	@SerializedName(value = "ticket-number")
	private String ticketNumber;
	
	@ApiModelProperty(dataType = "String", notes = "CPF/CNPJ Completo Beneficiário", required = true)
	@SerializedName(value = "assignor-document-number")
	private String assignorDocumentNumber;
	
	@ApiModelProperty(dataType = "Integer", notes = "Dígito de Controle CPF/CNPJ Beneficiário", required = true)
	@SerializedName(value = "assignor-document-type")
	private Integer assignorDocumentType;
	
	@ApiModelProperty(dataType = "String", notes = "Data de Emissão do Título (Formato: DD.MM.AAAA)", required = true)
	@SerializedName(value = "emission-date")
	private String emissionDate;
	
	@ApiModelProperty(dataType = "String", notes = "Data de Vencimento do Título (Formato: DD.MM.AAAA)", required = true)
	@SerializedName(value = "expiration-date")
	private String expirationDate;
	
	@ApiModelProperty(dataType = "Integer", notes = "Valor nominal do titulo", required = true)
	private float amount;
	
	@ApiModelProperty(dataType = "Integer", notes = "Código da Espécie do Título Códigos possíveis de acordo com item 9.1", required = true)
	private Integer type;
	
	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JsonManagedReference(value = "payment-customer")
	private List<Customer> custumer;
	
	@OneToMany(mappedBy = "payment", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Guarantor.class)
	@JsonManagedReference(value = "payment-guarantor")
	private List<Guarantor> guarantor;
	
	@ApiModelProperty(dataType = "Short", notes = "Define o número de dias decorrentes, após a data de vencimento, para inicialização do processo de cobrança, via protesto, do Título de Cobrança.", required = false)
	@SerializedName(value = "days-to-protest")
	private short daysToProtest;
	
	@ApiModelProperty(dataType = "String", notes = "Data indicativa do início da cobrança de multa para o Título de Cobrança")
	@SerializedName(value = "days-to-fine")
	private String daysToFine;
	
	@ApiModelProperty(dataType = "Double", notes = "Define o percentual de multa que será aplicado, em caso de atraso no pagamento, ao Título de Cobrança.")
	@SerializedName(value = "fine-rate")
	private double fineRate;
	
	@ApiModelProperty(dataType = "Double", notes = "Define o valor de multa que será aplicado, em caso de atraso no pagamento, ao Título de Cobrança.")
	@SerializedName(value = "fine-amount")
	private double fineAmount;
	
	@SerializedName(value = "interest-rate")
	private String interestRate;
	
	@SerializedName(value = "interest-amount")
	private String interestAmout;
	
	private StatusEnum status;

	public Payment(Integer id, String clientid, String clientsecret, String originProduct, String originRequestid, Provider provider, 
			String ticketNumber, String assignorDocumentNumber, Integer assignorDocumentType, String emissionDate, 
			String expirationDate, float amount, Integer type, List<Customer> custumer, List<Guarantor> guarantor, 
			short daysToProtest, String daysToFine, double fineRate, double fineAmount, String interestRate, 
			String interestAmout, StatusEnum status) {
		this.id = id;
		this.clientid = clientid;
		this.clientsecret = clientsecret;
		this.originProduct = originProduct;
		this.originRequestid = originRequestid;
		this.provider = provider;
		this.ticketNumber = ticketNumber;
		this.assignorDocumentNumber = assignorDocumentNumber;
		this.assignorDocumentType = assignorDocumentType;
		this.emissionDate = emissionDate;
		this.expirationDate = expirationDate;
		this.amount = amount;
		this.type = type;
		this.custumer = custumer;
		this.guarantor = guarantor;
		this.daysToProtest = daysToProtest;
		this.daysToFine = daysToFine;
		this.fineRate = fineRate;
		this.fineAmount = fineAmount;
		this.interestRate = interestRate;
		this.interestAmout = interestAmout;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getClientsecret() {
		return clientsecret;
	}

	public void setClientsecret(String clientsecret) {
		this.clientsecret = clientsecret;
	}

	public String getOriginProduct() {
		return originProduct;
	}

	public void setOriginProduct(String originProduct) {
		this.originProduct = originProduct;
	}

	public String getOriginRequestid() {
		return originRequestid;
	}

	public void setOriginRequestid(String originRequestid) {
		this.originRequestid = originRequestid;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getAssignorDocumentNumber() {
		return assignorDocumentNumber;
	}

	public void setAssignorDocumentNumber(String assignorDocumentNumber) {
		this.assignorDocumentNumber = assignorDocumentNumber;
	}

	public Integer getAssignorDocumentType() {
		return assignorDocumentType;
	}

	public void setAssignorDocumentType(Integer assignorDocumentType) {
		this.assignorDocumentType = assignorDocumentType;
	}

	public String getEmissionDate() {
		return emissionDate;
	}

	public void setEmissionDate(String emissionDate) {
		this.emissionDate = emissionDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<Customer> getCustumer() {
		return custumer;
	}

	public void setCustumer(List<Customer> custumer) {
		this.custumer = custumer;
	}

	public List<Guarantor> getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(List<Guarantor> guarantor) {
		this.guarantor = guarantor;
	}

	public short getDaysToProtest() {
		return daysToProtest;
	}

	public void setDaysToProtest(short daysToProtest) {
		this.daysToProtest = daysToProtest;
	}

	public String getDaysToFine() {
		return daysToFine;
	}

	public void setDaysToFine(String daysToFine) {
		this.daysToFine = daysToFine;
	}

	public double getFineRate() {
		return fineRate;
	}

	public void setFineRate(double fineRate) {
		this.fineRate = fineRate;
	}

	public double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getInterestAmout() {
		return interestAmout;
	}

	public void setInterestAmout(String interestAmout) {
		this.interestAmout = interestAmout;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
}