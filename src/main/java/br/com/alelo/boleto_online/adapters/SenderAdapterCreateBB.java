package br.com.alelo.boleto_online.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;

import br.com.alelo.boleto_online.ports.ISenderPort;

public class SenderAdapterCreateBB implements ISenderPort {

	@Autowired
	private JmsTemplate jmsTemplate;
		
	private static final Logger LOGGER = LoggerFactory.getLogger(SenderAdapterCreateBB.class);
	
	@Value("${application.fila.jms.bnk_register_bb_new}")
	private String filaRegisterBBNew;
	
	@Override
    public void send(String message){
		LOGGER.info("Passando novo boleto para a fila: bnk.register.bb.new" );
    	jmsTemplate.convertAndSend(filaRegisterBBNew, message);
        LOGGER.info("Passou a seguinte mensagem: \n" + message);
    }
}