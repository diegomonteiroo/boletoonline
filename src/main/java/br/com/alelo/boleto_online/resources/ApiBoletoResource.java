package br.com.alelo.boleto_online.resources;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.alelo.boleto_online.core.domain.Payment;
import br.com.alelo.boleto_online.core.services.ApiBoletoService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/apiboleto")
public class ApiBoletoResource {

	@Autowired
	private ApiBoletoService apiService;
	
	@ApiOperation(value = "Salvar um Boleto", nickname = "Validar banco e salvar registro no banco de dados.", response = Payment.class )
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Payment> save(@RequestBody @Valid Payment apiBoleto){
		try {
			Payment paymentValid;
			paymentValid = apiService.filter(apiBoleto);
			if (Objects.nonNull(paymentValid))
				return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}