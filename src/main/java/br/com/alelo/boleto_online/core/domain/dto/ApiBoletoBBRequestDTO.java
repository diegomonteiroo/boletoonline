package br.com.alelo.boleto_online.core.domain.dto;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.google.gson.annotations.SerializedName;

import br.com.alelo.boleto_online.core.domain.Payment;

public class ApiBoletoBBRequestDTO {
	@SerializedName(value = "payment-assignor-document-number")
	private String paymentAssignorDocumentNumber;
	
	@SerializedName(value = "branch-document-number")
	private String branchDocumentNumber;
	
	@SerializedName(value = "payment-assignor-document-type")
	private Integer paymentAssisgnorDocumentType;
	
	@SerializedName(value = "access-type")
	private Integer accessType;
	
	@SerializedName(value = "payment-provider-portfolio")
	private Integer paymentProviderPortfolio;
	
	@SerializedName(value = "financial-title-variation")
	private short financialTitleVariation;
	
	@SerializedName(value = "financial-code-title")
	private short financialCodeTitle;
	
	@SerializedName(value = "negociation-number")
	private String negociationNumber;
	
	@SerializedName(value = "bank-ticket-type")
	private Integer banckTicketType;
	
	@SerializedName(value = "client-number")
	private String clientNumber;
	
	@SerializedName(value = "payment-emission-date")
	private String paymentEmissionDate;
	
	@SerializedName(value = "payment-expiration-date")
	private String paymentExpirationDate;
	
	@SerializedName(value = "expiration-date")
	private Integer expirationDate;
	
	@SerializedName(value = "mode-title-code")
	private short modeTitleCode;
	
	@SerializedName(value = "payment-amount")
	private Integer paymentAmout;
	
	@SerializedName(value = "discount-type-code")
	private short discountTypeCode;
	
	@SerializedName(value = "interest-type")
	private Integer interestType;
	
	@SerializedName(value = "fine-type-code")
	private Integer fineTypeCode;
	
	@SerializedName(value = "accept-code")
	private String acceptCode;
	
	@SerializedName(value = "financial-type-code")
	private short financialTypeCode;
	
	@SerializedName(value = "partial-receipt")
	private short partialReceipt;
	
	@SerializedName(value = "customer-name")
	private String customerName;
	
	@SerializedName(value = "customer-address-street")
	private String customerAddressStreet;
	
	@SerializedName(value = "customer-address-number")
	private String customerAddressNumber;
	
	@SerializedName(value = "customer-address-zipcode")
	private Integer customerAddressZipcode;
	
	@SerializedName(value = "customer-address-complement")
	private Integer customerAddressComplement;
	
	@SerializedName(value = "customer-address-district")
	private String customerAddressDistrict;
	
	@SerializedName(value = "customer-address-city")
	private String customerAddressCity;
	
	@SerializedName(value = "customer-address-state")
	private String customerAddressState;
	
	@SerializedName(value = "customer-document-type")
	private Integer customerDocumentType;
	
	@SerializedName(value = "customer-document-number")
	private Integer customerDocumentNumber;
	
	@SerializedName(value = "bank-ticket-type-number")
	private Integer banckTicketTypeNumber;
	
	@SerializedName(value = "user-registration")
	private String userRegistration;
	
	@SerializedName(value = "request-channel-code-type")
	private Integer requestChannelCodeType;
	
	@SerializedName(value = "document-type")
	private short documentType;
	
	@SerializedName(value = "document-number")
	private Integer documentNumber;
	
	@SerializedName(value = "guarantor-address-street")
	private String guarantorAddressStreet;
	
	@SerializedName(value = "guarantor-address-number")
	private String guarantorAddressNumber;

	@SerializedName(value = "guarantor-address-zipcode")
	private String guarantorAddressZipcode;
	
	@SerializedName(value = "guarantor-address-complement")
	private String guarantorAddressComplement;
	
	@SerializedName(value = "guarantor-address-district")
	private String guarantorAddressDistrict;
	
	@SerializedName(value = "guarantor-address-city")
	private String guarantorAddressCity;
	
	@SerializedName(value = "guarantor-address-state")
	private String guarantorAddressState;
	
	@SerializedName(value = "guarantor-document-type")
	private Integer guarantorDocumetType;
	
	@SerializedName(value = "guarantor-document-number")
	private Integer guarantorDocumentNumber;
	
	@SerializedName(value = "payment-days-to-protest")
	private short paymentDaysToProtest;
	
	@SerializedName(value = "payment-days-to-fine")
	private String paymentDaysToFine;
	
	@SerializedName(value = "payment-fine-rate")
	private double paymentFIneRate;
	
	@SerializedName(value = "payment-fine-amount")
	private double paymentFineAmout;
	
	
	
	public ApiBoletoBBRequestDTO(Payment boleto) {
		
	}

}