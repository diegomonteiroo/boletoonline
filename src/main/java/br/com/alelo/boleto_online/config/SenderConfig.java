package br.com.alelo.boleto_online.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;

import br.com.alelo.boleto_online.adapters.SenderAdapterCreateBB;
import br.com.alelo.boleto_online.adapters.SenderAdapterCreateBrad;

@Configuration
@EnableJms
public class SenderConfig {
    @Value("${activemq.broker-url}")
    private String brokerUrl;

    @Bean
    public ActiveMQConnectionFactory senderActiveMQConnectionFactory(){
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);

        return activeMQConnectionFactory;
    }

    @Bean
    public CachingConnectionFactory cachingConnectionFactory(){
        return new CachingConnectionFactory(senderActiveMQConnectionFactory());
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        return new JmsTemplate(cachingConnectionFactory());
    }

    @Bean
    public SenderAdapterCreateBB senderBB(){
        return new SenderAdapterCreateBB();
    }
    
    @Bean
    public SenderAdapterCreateBrad senderBrad() {
    	return new SenderAdapterCreateBrad();
    }
}