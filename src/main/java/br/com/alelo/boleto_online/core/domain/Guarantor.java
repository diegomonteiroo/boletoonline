package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "guarantor")
public class Guarantor implements Serializable {

	private static final long serialVersionUID = -2057450885158411871L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JsonBackReference(value = "payment-guarantor")
	private Payment payment;
	
	@OneToOne(mappedBy = "guarantor")
	private Address address;

	@OneToOne(mappedBy = "guarantor")
	private Document document;

	public Guarantor(Integer id, Payment payment, Address address, Document document) {
		super();
		this.id = id;
		this.payment = payment;
		this.address = address;
		this.document = document;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
}