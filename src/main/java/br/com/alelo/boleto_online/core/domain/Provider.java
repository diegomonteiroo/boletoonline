package br.com.alelo.boleto_online.core.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "provider")
public class Provider implements Serializable{

	private static final long serialVersionUID = 2817482465890844616L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ApiModelProperty(dataType = "String", notes = "Informar numero do Banco. 001 - banco do brasil ou 237 - banco bradesco", example = "001")
	private String provider;
	
	@ApiModelProperty(dataType = "String", notes = "Informar agencia", example = "1234", required = true)
	private String agency;
	
	@ApiModelProperty(dataType = "Integer", notes = "Informar digito da agencia", example = "1", required = false)
	private Integer agencydigit;
	
	@ApiModelProperty(dataType = "Long", notes = "Informar o numero da conta", example = "222222", required = true)
	private Long account;
	
	@ApiModelProperty(dataType = "Integer", notes = "Informar digito da conta", example = "1", required = true)
	private Integer accountdigit;
	
	@ApiModelProperty(dataType = "String", notes = "Informar modalidade da carteira. 09 - Cobrança escritural, 05 - Cobrança de Seguros", example = "09", required = true)
	private String portfolio;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Payment payment;

	public Provider(Integer id, String provider, String agency, Integer agencydigit, Long account, Integer accountdigit,
			String portfolio, Payment payment) {
		super();
		this.id = id;
		this.provider = provider;
		this.agency = agency;
		this.agencydigit = agencydigit;
		this.account = account;
		this.accountdigit = accountdigit;
		this.portfolio = portfolio;
		this.payment = payment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public Integer getAgencydigit() {
		return agencydigit;
	}

	public void setAgencydigit(Integer agencydigit) {
		this.agencydigit = agencydigit;
	}

	public Long getAccount() {
		return account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	public Integer getAccountdigit() {
		return accountdigit;
	}

	public void setAccountdigit(Integer accountdigit) {
		this.accountdigit = accountdigit;
	}

	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}
}