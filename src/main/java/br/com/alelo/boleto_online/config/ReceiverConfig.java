package br.com.alelo.boleto_online.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

import br.com.alelo.boleto_online.adapters.ReceiverAdapterBBError;
import br.com.alelo.boleto_online.adapters.ReceiverAdapterBBSuccess;
import br.com.alelo.boleto_online.adapters.ReceiverAdapterBradError;
import br.com.alelo.boleto_online.adapters.ReceiverAdapterBradSuccess;

@Configuration
@EnableJms
public class ReceiverConfig {

    @Value("${activemq.broker-url}")
    private String brokerUrl;

    @Bean
    public ActiveMQConnectionFactory receiverActiveMQConnectionFactory(){
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(){
        DefaultJmsListenerContainerFactory factory =  new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(receiverActiveMQConnectionFactory());
        return factory;
    }

    @Bean
    public ReceiverAdapterBBSuccess receiverSuccessBB(){
        return new ReceiverAdapterBBSuccess();
    }
    
    @Bean
    public ReceiverAdapterBBError receiverErrorBB() {
    	return new ReceiverAdapterBBError();
    }
    
    @Bean
    public ReceiverAdapterBradSuccess receiverSuccessBrad() {
    	return new ReceiverAdapterBradSuccess();
    }
    
    @Bean
    public ReceiverAdapterBradError receiverErrorBrad(){
    	return new ReceiverAdapterBradError();
    }
}